/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.jpa.repository.test;

import com.payconiq.stockmanager.entities.Stock;
import com.payconiq.stockmanager.jpa.repository.StockJpaRepository;
import java.math.BigDecimal;
import org.joda.time.LocalDateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ceowit
 */
@DataJpaTest
@RunWith(SpringRunner.class)
public class StockJpaRepositoryTest {

    @Autowired
    StockJpaRepository stockJpaRepository;

    /**
     * Test repository save method
     */
    @Test
    @Transactional
    public void testSave() {
        Stock stock = new Stock();
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setName("Stock A");
        stock.setLastUpdate(LocalDateTime.now().toDate());
        stock = this.stockJpaRepository.save(stock);
        Assert.assertNotNull(stock);
        Assert.assertNotNull(stock.getCurrentPrice());
        Assert.assertNotNull(stock.getName());
        Assert.assertNotNull(stock.getLastUpdate());
        Assert.assertNotNull(stock.getId());
        Assert.assertEquals("Stock A", this.stockJpaRepository.findById(stock.getId()).get().getName());
    }

    /**
     * Find stock by ID
     */
    @Test
    @Transactional
    public void findByID() {
        Stock stock = new Stock();
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setName("Stock A");
        stock.setLastUpdate(LocalDateTime.now().toDate());
        stock = this.stockJpaRepository.save(stock);
        Assert.assertEquals("Stock A", this.stockJpaRepository.findById(stock.getId()).get().getName());
    }

    @Test
    @Transactional
    public void findAll() {
        Stock stock = new Stock();
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setName("Stock A");
        stock.setLastUpdate(LocalDateTime.now().toDate());
        this.stockJpaRepository.save(stock);
        stock = new Stock();
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setName("Stock B");
        stock.setLastUpdate(LocalDateTime.now().toDate());
        this.stockJpaRepository.save(stock);
        stock = new Stock();
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setName("Stock C");
        stock.setLastUpdate(LocalDateTime.now().toDate());
        this.stockJpaRepository.save(stock);
        stock = new Stock();
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setName("Stock D");
        stock.setLastUpdate(LocalDateTime.now().toDate());
        this.stockJpaRepository.save(stock);
        Assert.assertEquals(4, this.stockJpaRepository.findAll().size());
    }

}
