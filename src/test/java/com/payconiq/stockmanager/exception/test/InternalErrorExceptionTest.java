/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.exception.test;

import com.payconiq.stockmanager.exception.InternalErrorException;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;

/**
 *
 * @author ceowit
 */
public class InternalErrorExceptionTest {
    
    @Test
    public void testException(){
        InternalErrorException internalErrorException = new InternalErrorException(HttpStatus.INTERNAL_SERVER_ERROR,"VALIDATION_ERROR","Validation error");
        Assert.assertNotNull(internalErrorException);
        Assert.assertNotNull(internalErrorException.getErrorcode());
        Assert.assertNotNull(internalErrorException.getHttpStatus());
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,internalErrorException.getHttpStatus());
    }

    
    @Test
    public void testExceptionFailed(){
        InternalErrorException internalErrorException = new InternalErrorException(HttpStatus.INTERNAL_SERVER_ERROR,"VALIDATION_ERROR","Validation error");
        Assert.assertNotNull(internalErrorException);
        Assert.assertNotNull(internalErrorException.getErrorcode());
        Assert.assertNotNull(internalErrorException.getHttpStatus());
        Assert.assertNotEquals(HttpStatus.BAD_REQUEST,internalErrorException.getHttpStatus());
    }

    
}
