/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.operation.beans.test;

import com.payconiq.stockmanager.api.AbstractResponse;
import com.payconiq.stockmanager.entities.Stock;
import com.payconiq.stockmanager.exception.InternalErrorException;
import com.payconiq.stockmanager.jpa.repository.StockJpaRepository;
import com.payconiq.stockmanager.operation.StockManagerOperation;
import io.jsonwebtoken.JwtException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.joda.time.LocalDateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author ceowit
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class StockManagerOperationBeanTest {

    @Autowired
    StockManagerOperation stockManagerOperation;

    @MockBean
    StockJpaRepository stockJpaRepository;

    @Test
    public void testStockByIdNoContent() throws InternalErrorException {
        try {
            ResponseEntity<AbstractResponse<Stock>> response = this.stockManagerOperation.getStockById("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA", Long.getLong("1"));
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.NO_CONTENT);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }

    @Test
    public void testStockByIdOK() throws InternalErrorException {
        try {
            Stock stock = new Stock();
            stock.setName("Stock A");
            stock.setCurrentPrice(BigDecimal.TEN);
            stock.setLastUpdate(LocalDateTime.now().toDate());

            Mockito.when(this.stockJpaRepository.findById(Mockito.any(Long.class))).thenReturn(Optional.of(stock));

            ResponseEntity<AbstractResponse<Stock>> response = this.stockManagerOperation.getStockById("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA", Long.parseLong("1"));
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.OK);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }

    @Test
    public void testStockByIdNotFound() throws InternalErrorException {
        try {
            Mockito.when(this.stockJpaRepository.findById(Mockito.any(Long.class))).thenReturn(null);
            ResponseEntity<AbstractResponse<Stock>> response = this.stockManagerOperation.getStockById("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA", Long.parseLong("1"));
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.NOT_FOUND);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }

    @Test
    public void testGetStocksOK() throws InternalErrorException {

        List<Stock> stocks = new ArrayList<>();

        Stock stock = new Stock();
        stock.setName("Stock A");
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setLastUpdate(LocalDateTime.now().toDate());
        stocks.add(stock);

        stock = new Stock();
        stock.setName("Stock B");
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setLastUpdate(LocalDateTime.now().toDate());
        stocks.add(stock);

        stock = new Stock();
        stock.setName("Stock C");
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setLastUpdate(LocalDateTime.now().toDate());
        stocks.add(stock);

        Mockito.when(this.stockJpaRepository.findAll()).thenReturn(stocks);

        try {
            ResponseEntity<AbstractResponse<List<Stock>>> response = this.stockManagerOperation.getStocks("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA");
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.OK);
            Assert.assertEquals(false, response.getBody().getData().isEmpty());
            Assert.assertEquals(3, response.getBody().getData().size());
        } catch (InternalErrorException ex) {
            throw ex;
        }

    }

    @Test
    public void testGetStocksNotFound() throws InternalErrorException {
        Mockito.when(this.stockJpaRepository.findAll()).thenReturn(null);
        try {
            ResponseEntity<AbstractResponse<List<Stock>>> response = this.stockManagerOperation.getStocks("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA");
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.NOT_FOUND);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }

    @Test(expected = JwtException.class)
    public void testGetStocksUnAuthorized() throws InternalErrorException {
        Mockito.when(this.stockJpaRepository.findAll()).thenReturn(null);
        try {
            ResponseEntity<AbstractResponse<List<Stock>>> response = this.stockManagerOperation.getStocks("eyJhbGciOiJIUzUxMiJ9.eyJhdXsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA");
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.NOT_FOUND);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }

    @Test
    public void updateStockOK() throws InternalErrorException {
        Stock stock = new Stock();
        stock.setName("Stock A");
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setLastUpdate(LocalDateTime.now().toDate());

        Mockito.when(this.stockJpaRepository.findById(Mockito.any(Long.class))).thenReturn(Optional.of(stock));
        Mockito.when(this.stockJpaRepository.save(Mockito.any())).thenReturn(stock);

        try {
            ResponseEntity<AbstractResponse<Stock>> response = this.stockManagerOperation.updateStock("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA", Long.parseLong("1"), stock);
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.OK);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }

    @Test
    public void updateStockBadRequest() throws InternalErrorException {
        Stock stock = new Stock();
        stock.setName("Stock A");
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setLastUpdate(LocalDateTime.now().toDate());

        Mockito.when(this.stockJpaRepository.findById(Mockito.any(Long.class))).thenReturn(null);
        Mockito.when(this.stockJpaRepository.save(Mockito.any())).thenReturn(stock);
        try {
            ResponseEntity<AbstractResponse<Stock>> response = this.stockManagerOperation.updateStock("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA", Long.parseLong("1"), null);
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.BAD_REQUEST);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }

    @Test
    public void updateStockNoFound() throws InternalErrorException {
        Stock stock = new Stock();
        stock.setName("Stock A");
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setLastUpdate(LocalDateTime.now().toDate());

        Mockito.when(this.stockJpaRepository.findById(Mockito.any(Long.class))).thenReturn(null);
        Mockito.when(this.stockJpaRepository.save(Mockito.any())).thenReturn(stock);
        try {
            ResponseEntity<AbstractResponse<Stock>> response = this.stockManagerOperation.updateStock("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA", Long.parseLong("1"), stock);
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.NOT_FOUND);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }

    @Test
    public void addStockOK() throws InternalErrorException {
        Stock stock = new Stock();
        stock.setName("Stock A");
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setLastUpdate(LocalDateTime.now().toDate());
        Mockito.when(this.stockJpaRepository.save(Mockito.any())).thenReturn(stock);

        try {
            ResponseEntity<AbstractResponse<Stock>> response = this.stockManagerOperation.addStock("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA", stock);
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.OK);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }

    @Test
    public void addStockBadRequest() throws InternalErrorException {
        Stock stock = new Stock();
        stock.setName("Stock A");
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setLastUpdate(LocalDateTime.now().toDate());
        Mockito.when(this.stockJpaRepository.save(Mockito.any())).thenReturn(stock);

        try {
            ResponseEntity<AbstractResponse<Stock>> response = this.stockManagerOperation.addStock("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA", null);
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.BAD_REQUEST);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }
    
    @Test
    public void addStockBadRequestNoName() throws InternalErrorException {
        Stock stock = new Stock();
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setLastUpdate(LocalDateTime.now().toDate());
        Mockito.when(this.stockJpaRepository.save(Mockito.any())).thenReturn(stock);

        try {
            ResponseEntity<AbstractResponse<Stock>> response = this.stockManagerOperation.addStock("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA", stock);
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.BAD_REQUEST);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }

    @Test
    public void addStockBadRequestNoAmount() throws InternalErrorException {
        Stock stock = new Stock();
        stock.setName("Stock A");
        stock.setLastUpdate(LocalDateTime.now().toDate());
        Mockito.when(this.stockJpaRepository.save(Mockito.any())).thenReturn(stock);

        try {
            ResponseEntity<AbstractResponse<Stock>> response = this.stockManagerOperation.addStock("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA", stock);
            Assert.assertEquals(true, response.getStatusCode() == HttpStatus.BAD_REQUEST);
        } catch (InternalErrorException ex) {
            throw ex;
        }
    }

    
}
