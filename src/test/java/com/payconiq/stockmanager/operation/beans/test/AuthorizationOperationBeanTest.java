/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.operation.beans.test;

import com.payconiq.stockmanager.api.AbstractResponse;
import com.payconiq.stockmanager.exception.InternalErrorException;
import com.payconiq.stockmanager.operation.AuthorizationOperation;
import io.jsonwebtoken.JwtException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author ceowit
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class AuthorizationOperationBeanTest {

    @Autowired
    AuthorizationOperation authorizationOperation;

    @Test
    public void testAuthorization() throws InternalErrorException {
        ResponseEntity<AbstractResponse<String>> response = this.authorizationOperation.authorization("test@gmail.com", "12345");
        Assert.assertEquals(true, response.getStatusCode() == HttpStatus.OK);
    }

    @Test
    public void testValidateAuthorizationOK() throws InternalErrorException{
        Boolean validate = this.authorizationOperation.authorization("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA");
        Assert.assertTrue(validate);
    }

    @Test(expected = JwtException.class)
    public void testValidateAuthorizationUnauthorized() throws InternalErrorException{
        Boolean validate = this.authorizationOperation.authorization("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA");
        Assert.assertTrue(validate);
    }

    
}
