/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.api.test;

import com.payconiq.stockmanager.api.AbstractResponse;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author ceowit
 */
public class AbstractResponseTest {
 
    @Test
    public void testResponse(){
        AbstractResponse<String> abstractResponse = new AbstractResponse<>();
        abstractResponse.setData("Hello");
        abstractResponse.setMessage("Response sent successfully");
        abstractResponse.setStatus(Boolean.TRUE);
        Assert.assertNotNull(abstractResponse);
        Assert.assertNotNull(abstractResponse.getData());
        Assert.assertNotNull(abstractResponse.getMessage());
        Assert.assertNotNull(abstractResponse.getStatus());
        Assert.assertEquals(Boolean.TRUE,abstractResponse.getStatus());
        Assert.assertEquals("Hello",abstractResponse.getData());
    }
    
}