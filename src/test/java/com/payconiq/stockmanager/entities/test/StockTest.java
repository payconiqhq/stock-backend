/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.entities.test;

import com.payconiq.stockmanager.entities.Stock;
import java.math.BigDecimal;
import org.joda.time.LocalDateTime;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author ceowit
 */
public class StockTest {
    
    @Test
    public void validateEntity(){
        Stock stock = new Stock();
        stock.setCurrentPrice(BigDecimal.valueOf(100));
        stock.setId(Long.valueOf(1));
        stock.setLastUpdate(LocalDateTime.now().toDate());
        stock.setName("Facebook Stock");
        Assert.assertNotNull(stock);
        Assert.assertNotNull(stock.getId());
        Assert.assertNotNull(stock.getCurrentPrice());
        Assert.assertNotNull(stock.getLastUpdate());
        Assert.assertNotNull(stock.getName());
        Assert.assertEquals("Facebook Stock", stock.getName());
        Assert.assertEquals(BigDecimal.valueOf(100), stock.getCurrentPrice());
    }
    
}
