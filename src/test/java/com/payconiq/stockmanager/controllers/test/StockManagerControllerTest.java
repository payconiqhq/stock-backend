/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.controllers.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.payconiq.stockmanager.api.AbstractResponse;
import com.payconiq.stockmanager.controllers.StockManagerController;
import com.payconiq.stockmanager.entities.Stock;
import com.payconiq.stockmanager.operation.AuthorizationOperation;
import com.payconiq.stockmanager.operation.StockManagerOperation;
import java.math.BigDecimal;
import org.joda.time.LocalDateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author ceowit
 */
@RunWith(SpringRunner.class)
@WebMvcTest(StockManagerController.class)
public class StockManagerControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    StockManagerOperation stockManagerOperation;

    @MockBean
    AuthorizationOperation authorizationOperation;

    /**
     *
     * @throws Exception
     */
    @Test
    public void authorizeOK() throws Exception {
        AbstractResponse<String> abstractResponse = new AbstractResponse<>();
        abstractResponse.setData("eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA");
        abstractResponse.setStatus(Boolean.TRUE);
        abstractResponse.setMessage("Authorization successful");
        Mockito.when(this.authorizationOperation.authorization(Mockito.any(), Mockito.any())).thenReturn(ResponseEntity.ok(abstractResponse));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/stocks/authorize?emailaddress=test@gmail.com&password=ABc123456!"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.status").value(true));
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void addStockOK() throws Exception {
        Stock stock = new Stock();
        stock.setName("Stock A");
        stock.setCurrentPrice(BigDecimal.TEN);

        AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
        abstractResponse.setStatus(Boolean.TRUE);
        abstractResponse.setMessage("Authorization successful");
        Mockito.when(this.stockManagerOperation.addStock(Mockito.any(), Mockito.any())).thenReturn(ResponseEntity.ok(abstractResponse));

        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/api/stocks")
                .content(objectMapper.writeValueAsString(stock))
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.status").value(true));
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void addStockBadRequest() throws Exception {
        Stock stock = new Stock();
        stock.setName("Stock A");

        AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
        abstractResponse.setStatus(Boolean.FALSE);
        abstractResponse.setMessage("Add stock failed");
        Mockito.when(this.stockManagerOperation.addStock(Mockito.any(), Mockito.any())).thenReturn(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(abstractResponse));

        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/api/stocks")
                .content(objectMapper.writeValueAsString(stock))
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void getStockByIdNotFound() throws Exception {
        AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
        abstractResponse.setStatus(Boolean.FALSE);
        abstractResponse.setMessage("Get Stocks failed");
        Mockito.when(this.stockManagerOperation.getStockById(Mockito.any(), Mockito.any())).thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(abstractResponse));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/stocks/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA"))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(false));
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void getStockByIdOK() throws Exception {
        Stock stock = new Stock();
        stock.setName("Stock A");
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setLastUpdate(LocalDateTime.now().toDate());

        AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
        abstractResponse.setStatus(Boolean.TRUE);
        abstractResponse.setData(stock);
        abstractResponse.setMessage("Authorization successful");
        Mockito.when(this.stockManagerOperation.getStockById(Mockito.any(), Mockito.any())).thenReturn(ResponseEntity.ok(abstractResponse));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/stocks/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(true));
    }

    /**
     * 
     * @throws Exception 
     */
    @Test
    public void getStockByIdNoContent() throws Exception {
        AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
        abstractResponse.setStatus(Boolean.FALSE);
        abstractResponse.setMessage("Authorization successful");
        Mockito.when(this.stockManagerOperation.getStockById(Mockito.any(), Mockito.any())).thenReturn(ResponseEntity.status(HttpStatus.NO_CONTENT).body(abstractResponse));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/stocks/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU1ODM5MzYxN30.UIDRiwHBp2KKh_yUQyoyhgptNZbN7T9pAXxMDpLZCXpELVE1Vv84ppiYV-abUUX6bxAf3meIN6jnmFsM8EaxNw"))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(false));
    }

    /**
     * 
     * @throws Exception 
     */
    @Test
    public void updateStockOK() throws Exception {
        Stock stock = new Stock();
        stock.setName("Stock A");
        stock.setCurrentPrice(BigDecimal.TEN);

        AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
        abstractResponse.setStatus(Boolean.TRUE);
        abstractResponse.setMessage("Authorization successful");
        Mockito.when(this.stockManagerOperation.updateStock(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(ResponseEntity.ok(abstractResponse));

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.put("/api/stocks/1")
                .content(objectMapper.writeValueAsString(stock))
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU1ODM5MzYxN30.UIDRiwHBp2KKh_yUQyoyhgptNZbN7T9pAXxMDpLZCXpELVE1Vv84ppiYV-abUUX6bxAf3meIN6jnmFsM8EaxNw"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.status").value(true));
    }

    /**
     * 
     * @throws Exception 
     */
    @Test
    public void updateStockBadRequest() throws Exception {
        Stock stock = new Stock();
        stock.setName("Stock A");

        AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
        abstractResponse.setStatus(Boolean.FALSE);
        abstractResponse.setMessage("Update stock failed");
        Mockito.when(this.stockManagerOperation.updateStock(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(abstractResponse));

        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.put("/api/stocks/1")
                .content(objectMapper.writeValueAsString(stock))
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpemF0aW9ua2V5IjoiQUJjMTIzNDU2ISIsImV4cCI6MTU2NzE5NDM2NH0._arNmf4iPbdces1LKjJwHgi31P6KHmaMx9K4z4xqolIE_D_xmaW2C36FAMSQXlF4b9o3l_P94lZulg25IrxeuA"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}
