/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.jpa.repository;

import com.payconiq.stockmanager.entities.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ceowit
 */
@Repository
public interface StockJpaRepository extends JpaRepository<Stock, Long> {

}