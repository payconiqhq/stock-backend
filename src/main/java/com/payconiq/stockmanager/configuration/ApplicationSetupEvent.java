/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.configuration;

import com.payconiq.stockmanager.entities.Stock;
import com.payconiq.stockmanager.jpa.repository.StockJpaRepository;
import java.math.BigDecimal;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ceowit
 */
@Component
public class ApplicationSetupEvent implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    StockJpaRepository stockJpaRepository;

    /**
     *
     * Setup list of stocks
     *
     * @param e
     */
    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent e) {
        Stock stock = new Stock();
        stock.setName("Stock A");
        stock.setCurrentPrice(BigDecimal.TEN);
        stock.setLastUpdate(LocalDateTime.now().toDate());
        this.stockJpaRepository.save(stock);
        stock = new Stock();
        stock.setName("Stock B");
        stock.setCurrentPrice(BigDecimal.valueOf(100));
        stock.setLastUpdate(LocalDateTime.now().toDate());
        this.stockJpaRepository.save(stock);
        stock = new Stock();
        stock.setName("Stock C");
        stock.setCurrentPrice(BigDecimal.valueOf(100));
        stock.setLastUpdate(LocalDateTime.now().toDate());
        this.stockJpaRepository.save(stock);
        stock = new Stock();
        stock.setName("Stock C");
        stock.setCurrentPrice(BigDecimal.valueOf(100));
        stock.setLastUpdate(LocalDateTime.now().toDate());
        this.stockJpaRepository.save(stock);
        stock = new Stock();
        stock.setName("Stock C");
        stock.setCurrentPrice(BigDecimal.valueOf(100));
        stock.setLastUpdate(LocalDateTime.now().toDate());
        this.stockJpaRepository.save(stock);
        stock = new Stock();
        stock.setName("Stock D");
        stock.setCurrentPrice(BigDecimal.valueOf(100));
        stock.setLastUpdate(LocalDateTime.now().toDate());
        this.stockJpaRepository.save(stock);
    }

}
