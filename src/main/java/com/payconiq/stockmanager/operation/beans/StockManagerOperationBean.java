/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.operation.beans;

import com.payconiq.stockmanager.api.AbstractResponse;
import com.payconiq.stockmanager.entities.Stock;
import com.payconiq.stockmanager.exception.InternalErrorException;
import com.payconiq.stockmanager.jpa.repository.StockJpaRepository;
import com.payconiq.stockmanager.operation.AuthorizationOperation;
import com.payconiq.stockmanager.operation.StockManagerOperation;
import java.util.List;
import java.util.Optional;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ceowit
 */
@Component
public class StockManagerOperationBean implements StockManagerOperation {

    @Autowired
    StockJpaRepository stockJpaRepository;

    @Autowired
    AuthorizationOperation authorizationOperation;

    private final Logger logger = LoggerFactory.getLogger(StockManagerOperationBean.class);

    /**
     *
     * Find a stock information by a stock ID
     *
     * @param authorization
     * @param stockId
     * @return
     * @throws InternalErrorException
     */
    @Override
    public ResponseEntity<AbstractResponse<Stock>> getStockById(String authorization, Long stockId) throws InternalErrorException {
        logger.trace("Fetching stock information by {}", stockId);
        AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
        if (!this.authorizationOperation.authorization(authorization)) {
            abstractResponse.setMessage("Authorization failed");
            abstractResponse.setStatus(Boolean.FALSE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(abstractResponse);
        }
        Optional<Stock> stock = this.stockJpaRepository.findById(stockId);
        if (stock == null) {
            logger.trace("No stock information found");
            abstractResponse.setMessage("No stock information found");
            abstractResponse.setStatus(Boolean.FALSE);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(abstractResponse);
        } else {
            if (stock.isPresent()) {
                logger.trace("Stock information fetched successfully");
                abstractResponse.setData(stock.get());
                abstractResponse.setMessage("Stock information fetched successfully");
                abstractResponse.setStatus(Boolean.TRUE);
                return ResponseEntity.ok(abstractResponse);
            } else {
                logger.trace("Stock information is not present");
                abstractResponse.setMessage("Stock information is not available");
                abstractResponse.setStatus(Boolean.FALSE);
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(abstractResponse);
            }
        }
    }

    /**
     *
     * Get all stocks, validated by an authorization token
     *
     * @param authorization
     * @return
     * @throws InternalErrorException
     */
    @Override
    public ResponseEntity<AbstractResponse<List<Stock>>> getStocks(String authorization) throws InternalErrorException {
        AbstractResponse<List<Stock>> abstractResponse = new AbstractResponse<>();
        if (!this.authorizationOperation.authorization(authorization)) {
            abstractResponse.setMessage("Authorization failed");
            abstractResponse.setStatus(Boolean.FALSE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(abstractResponse);
        }
        List<Stock> stocks = this.stockJpaRepository.findAll();
        if (stocks == null) {
            abstractResponse.setMessage("No stock information found");
            abstractResponse.setStatus(Boolean.FALSE);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(abstractResponse);
        } else {
            abstractResponse.setData(stocks);
            abstractResponse.setMessage("Stock information fetched successfully");
            abstractResponse.setStatus(Boolean.TRUE);
            return ResponseEntity.ok(abstractResponse);
        }
    }

    /**
     *
     * Update stock information, validated by an authorization token
     *
     * @param authorization
     * @param stockid
     * @param stock
     * @return
     * @throws InternalErrorException
     */
    @Override
    @Transactional
    public ResponseEntity<AbstractResponse<Stock>> updateStock(String authorization, Long stockid, Stock stock) throws InternalErrorException {
        AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
        if (!this.authorizationOperation.authorization(authorization)) {
            abstractResponse.setMessage("Authorization failed");
            abstractResponse.setStatus(Boolean.FALSE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(abstractResponse);
        }
        if (stock == null) {
            abstractResponse.setMessage("Stock information is required");
            abstractResponse.setStatus(Boolean.FALSE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(abstractResponse);
        }
        Optional<Stock> stockInfo = this.stockJpaRepository.findById(stockid);
        if (stockInfo == null || !stockInfo.isPresent()) {
            return ResponseEntity.notFound().build();
        } else {
            if (stockInfo.isPresent()) {
                Stock s = stockInfo.get();
                if (stock.getCurrentPrice() != null) {
                    s.setCurrentPrice(stock.getCurrentPrice());
                }
                if (stock.getName() != null) {
                    s.setName(stock.getName());
                }
                s.setLastUpdate(LocalDateTime.now().toDate());
                this.stockJpaRepository.save(s);
                abstractResponse.setData(s);
                abstractResponse.setMessage("Stock information updated");
                abstractResponse.setStatus(Boolean.TRUE);
                return ResponseEntity.ok(abstractResponse);
            } else {
                abstractResponse.setMessage("Stock information is not available");
                abstractResponse.setStatus(Boolean.TRUE);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(abstractResponse);
            }
        }
    }

    /**
     *
     * Add a new stock information, validated by a token
     *
     * @param authorization
     * @param stock
     * @return
     * @throws InternalErrorException
     */
    @Override
    @Transactional
    public ResponseEntity<AbstractResponse<Stock>> addStock(String authorization, Stock stock) throws InternalErrorException {
        AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
        if (!this.authorizationOperation.authorization(authorization)) {
            abstractResponse.setMessage("Authorization failed");
            abstractResponse.setStatus(Boolean.FALSE);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(abstractResponse);
        }
        if (stock == null) {
            abstractResponse.setMessage("Stock information is required");
            abstractResponse.setStatus(Boolean.TRUE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(abstractResponse);
        }
        if (stock.getCurrentPrice() == null) {
            abstractResponse.setMessage("Stock price is required");
            abstractResponse.setStatus(Boolean.TRUE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(abstractResponse);
        }
        if (stock.getName() == null) {
            abstractResponse.setMessage("Stock name is required");
            abstractResponse.setStatus(Boolean.TRUE);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(abstractResponse);
        }

        stock.setLastUpdate(LocalDateTime.now().toDate());
        stockJpaRepository.save(stock);
        abstractResponse.setData(stock);
        abstractResponse.setMessage("Stock information created");
        abstractResponse.setStatus(Boolean.TRUE);
        return ResponseEntity.ok(abstractResponse);
    }

}
