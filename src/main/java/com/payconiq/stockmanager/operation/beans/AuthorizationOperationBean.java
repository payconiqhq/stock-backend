/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.operation.beans;

import com.payconiq.stockmanager.api.AbstractResponse;
import com.payconiq.stockmanager.exception.InternalErrorException;
import com.payconiq.stockmanager.operation.AuthorizationOperation;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 *
 * @author ceowit
 */
@Component
public class AuthorizationOperationBean implements AuthorizationOperation {
    
    @Value("${authorization.key}")
    private String authorizationkey;
    
    @Value("${authorization.signing.key}")
    private String signingkey;
    
    @Override
    public ResponseEntity<AbstractResponse<String>> authorization(String emailaddress, String password) throws InternalErrorException {
        try {
            AbstractResponse<String> abstractResponse = new AbstractResponse<>();
            abstractResponse.setData(Jwts.builder().claim("authorizationkey", authorizationkey).signWith(SignatureAlgorithm.HS512, signingkey).setExpiration(LocalDateTime.now().plusDays(100).toDate()).compact());
            abstractResponse.setStatus(Boolean.TRUE);
            abstractResponse.setMessage("Authorization successful");
            return ResponseEntity.ok(abstractResponse);
        } catch (JwtException ex) {
            throw new InternalErrorException(HttpStatus.UNAUTHORIZED, "INVALID_TOKEN", "Token expired : ".concat(ex.getMessage()));
        }
    }
    
    @Override
    public Boolean authorization(String authorization) throws InternalErrorException {
        return Jwts.parser().setSigningKey(signingkey).parseClaimsJws(authorization).getBody().get("authorizationkey", String.class).equals(authorizationkey);
    }
    
}
