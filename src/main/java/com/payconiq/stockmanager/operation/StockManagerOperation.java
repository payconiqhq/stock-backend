/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.operation;

import com.payconiq.stockmanager.api.AbstractResponse;
import com.payconiq.stockmanager.entities.Stock;
import com.payconiq.stockmanager.exception.InternalErrorException;
import java.util.List;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author ceowit
 */
public interface StockManagerOperation {

    ResponseEntity<AbstractResponse<Stock>> getStockById(String authorization, Long stockId) throws InternalErrorException;

    ResponseEntity<AbstractResponse<List<Stock>>> getStocks(String authorization) throws InternalErrorException;

    ResponseEntity<AbstractResponse<Stock>> updateStock(String authorization,Long stockid, Stock stock) throws InternalErrorException;

    ResponseEntity<AbstractResponse<Stock>> addStock(String authorization, Stock stock) throws InternalErrorException;

}
