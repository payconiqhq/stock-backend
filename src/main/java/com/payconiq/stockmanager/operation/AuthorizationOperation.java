/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.operation;

import com.payconiq.stockmanager.api.AbstractResponse;
import com.payconiq.stockmanager.exception.InternalErrorException;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author ceowit
 */
public interface AuthorizationOperation {
    
    ResponseEntity<AbstractResponse<String>> authorization(String emailaddress,String password) throws InternalErrorException;
    
    Boolean authorization(String authorization) throws InternalErrorException;
    
}
