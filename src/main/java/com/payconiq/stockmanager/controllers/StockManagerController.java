/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.controllers;

import com.payconiq.stockmanager.api.AbstractResponse;
import com.payconiq.stockmanager.entities.Stock;
import com.payconiq.stockmanager.exception.InternalErrorException;
import com.payconiq.stockmanager.operation.AuthorizationOperation;
import com.payconiq.stockmanager.operation.StockManagerOperation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ceowit
 */
@RestController
@RequestMapping(value = "/api/stocks")
@Api(value = "Stock Manager", tags = {
    "Stock Manager"
}, description = "Stock APIs")
public class StockManagerController {

    @Autowired
    StockManagerOperation stockManagerOperation;

    @Autowired
    AuthorizationOperation authorizationOperation;

    /**
     *
     * Find a stock information by a stock ID
     *
     * @param authorization
     * @param stockid
     * @return
     */
    @ApiOperation(produces = "application/json", httpMethod = "GET", response = AbstractResponse.class, value = "Find Stock", notes = "Get stock information with id", tags = {"Stock Manager"})
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/{stockid}")
    public ResponseEntity<AbstractResponse<Stock>> getStockById(@RequestHeader(name = "Authorization", required = true) String authorization, @PathVariable(value = "stockid", required = true) Long stockid) {
        try {
            return this.stockManagerOperation.getStockById(authorization, stockid);
        } catch (InternalErrorException ex) {
            AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
            abstractResponse.setStatus(Boolean.FALSE);
            abstractResponse.setMessage(ex.getMessage());
            abstractResponse.setErrorcode(ex.getErrorcode());
            return ResponseEntity.status(ex.getHttpStatus()).body(abstractResponse);
        }
    }

    /**
     *
     * Get all stocks, validated by an authorization token
     *
     * @param authorization
     * @return
     */
    @ApiOperation(produces = "application/json", httpMethod = "GET", response = AbstractResponse.class, value = "Find all Stocks", notes = "Get all stock information", tags = {"Stock Manager"})
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AbstractResponse<List<Stock>>> getStocks(@RequestHeader(name = "Authorization", required = true) String authorization) {
        try {
            return this.stockManagerOperation.getStocks(authorization);
        } catch (InternalErrorException ex) {
            AbstractResponse<List<Stock>> abstractResponse = new AbstractResponse<>();
            abstractResponse.setStatus(Boolean.FALSE);
            abstractResponse.setMessage(ex.getMessage());
            abstractResponse.setErrorcode(ex.getErrorcode());
            return ResponseEntity.status(ex.getHttpStatus()).body(abstractResponse);
        }
    }

    /**
     *
     * Update stock information, validated by an authorization token
     *
     * @param authorization
     * @param stockid
     * @param stock
     * @return
     */
    @ApiOperation(produces = "application/json", consumes = "application/json", httpMethod = "PUT", response = AbstractResponse.class, value = "Update Stock", notes = "Update stock information with id", tags = {"Stock Manager"})
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, value = "/{stockid}")
    public ResponseEntity<AbstractResponse<Stock>> updateStock(@RequestHeader(name = "Authorization", required = true) String authorization, @PathVariable(value = "stockid", required = true) Long stockid, @RequestBody Stock stock) {
        try {
            return this.stockManagerOperation.updateStock(authorization, stockid, stock);
        } catch (InternalErrorException ex) {
            AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
            abstractResponse.setStatus(Boolean.FALSE);
            abstractResponse.setMessage(ex.getMessage());
            abstractResponse.setErrorcode(ex.getErrorcode());
            return ResponseEntity.status(ex.getHttpStatus()).body(abstractResponse);
        }
    }

    /**
     *
     * Add a new stock information, validated by a token
     *
     * @param authorization
     * @param stock
     * @return
     */
    @ApiOperation(produces = "application/json", consumes = "application/json", httpMethod = "POST", response = AbstractResponse.class, value = "Create Stock", notes = "Create stock information", tags = {"Stock Manager"})
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AbstractResponse<Stock>> addStock(@RequestHeader(name = "Authorization", required = true) String authorization, @RequestBody Stock stock) {
        try {
            return this.stockManagerOperation.addStock(authorization, stock);
        } catch (InternalErrorException ex) {
            AbstractResponse<Stock> abstractResponse = new AbstractResponse<>();
            abstractResponse.setStatus(Boolean.FALSE);
            abstractResponse.setMessage(ex.getMessage());
            abstractResponse.setErrorcode(ex.getErrorcode());
            return ResponseEntity.status(ex.getHttpStatus()).body(abstractResponse);
        }
    }

    /**
     *
     * Generate authorization token with the authorization key -- hypothetical
     * tokens
     *
     * @param emailaddress
     * @param password
     * @return
     */
    @ApiOperation(produces = "application/json", httpMethod = "GET", response = AbstractResponse.class, value = "Authorization", notes = "Generate authorization token", tags = {"Stock Manager"})
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/authorize")
    public ResponseEntity<AbstractResponse<String>> authorization(@RequestParam(name = "emailaddress", required = true) String emailaddress, @RequestParam(name = "password", required = true) String password) {
        try {
            return this.authorizationOperation.authorization(emailaddress, password);
        } catch (InternalErrorException ex) {
            AbstractResponse<String> abstractResponse = new AbstractResponse<>();
            abstractResponse.setStatus(Boolean.FALSE);
            abstractResponse.setMessage(ex.getMessage());
            abstractResponse.setErrorcode(ex.getErrorcode());
            return ResponseEntity.status(ex.getHttpStatus()).body(abstractResponse);
        }
    }

}
