/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payconiq.stockmanager.exception;

import org.springframework.http.HttpStatus;

/**
 *
 * @author ceowit
 */
public class InternalErrorException extends Exception{
    
    private final HttpStatus httpStatus;
    private final String errorcode;

    public InternalErrorException(HttpStatus httpStatus, String errorcode, String message) {
        super(message);
        this.httpStatus = httpStatus;
        this.errorcode = errorcode;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
    
}
