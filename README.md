# stock-backend

The stock manager is a Payconiq application that allows automatic stock listing and reporting, when required. Users can get the following features;

*  Add a new stock to the current listings
*  Fetch the list of all stock listings
*  Get the full information of a specific stock.
*  Update the information of a specific stock.


Merge Request to master from the dev branch

`git checkout master
git merge opeyemi_adeniran_dev`


Run the jar build using the command below;

`java -jar <file name>.jar`

The server starts on port 9800.